#!/usr/bin/env python
# -*- encoding: utf-8 -*-

servers = {
    'default': {
        'server_cmd': ['/home/dstan/cranspasswords/serverconfigs/tudor/cpasswords-server', ],
        'keep-alive': True, # <-- experimental, n'ouvre qu'une connexion
    }
}
